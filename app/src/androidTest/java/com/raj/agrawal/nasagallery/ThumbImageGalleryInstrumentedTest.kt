package com.raj.agrawal.nasagallery

import android.view.View
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewAssertion
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.raj.agrawal.nasagallery.ui.ThumbGalleryFragment
import org.hamcrest.Matchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ThumbImageGalleryInstrumentedTest {

    @Rule
    @JvmField
    var mInstantTaskExecutorRule = InstantTaskExecutorRule()

    @Test
    fun snackBarShouldNotAttachIfAppStartsInOnlineMode() {
        launchFragmentInContainer<ThumbGalleryFragment>(themeResId = R.style.Theme_NasaGalleryApp)

        onView(allOf(withId(com.google.android.material.R.id.snackbar_text), withText(R.string.no_internet)))
            .check(doesNotExist())
    }

    @Test
    fun shouldDisplayRecyclerView() {
        launchFragmentInContainer<ThumbGalleryFragment>(themeResId = R.style.Theme_NasaGalleryApp)

        onView(allOf(withId(R.id.gallery_list)))
            .check(matches(isDisplayed()))

        onView(withId(R.id.gallery_list)).check(RecyclerViewItemCountAssertion())
    }
}