package com.raj.agrawal.nasagallery.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Deserialize json-string into specified object-type
 */
inline fun <reified T> Gson.fromJson(json: String): T =
    fromJson(json, object : TypeToken<T>() {}.type)