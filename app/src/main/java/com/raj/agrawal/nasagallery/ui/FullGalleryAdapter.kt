package com.raj.agrawal.nasagallery.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.raj.agrawal.nasagallery.R
import com.raj.agrawal.nasagallery.data.entities.GalleryResponse
import com.raj.agrawal.nasagallery.databinding.LayoutGalleryFullBinding

class FullGalleryAdapter(private val dataSet: GalleryResponse, private val imageClick: (position: Int) -> Unit) :
    RecyclerView.Adapter<FullGalleryAdapter.ViewHolder>() {

    class ViewHolder(
        binding: LayoutGalleryFullBinding,
        imageClick: (position: Int) -> Unit
    ) : RecyclerView.ViewHolder(binding.root) {
        val context: Context = binding.root.context

        val imageView = binding.imageThumb
        val textTitle = binding.textTitle
        val textDescription = binding.textDescription
        val textDate = binding.textDate
        val textAuthor = binding.textAuthor
        val textGroup = binding.textGroup

        init {
            imageView.setOnClickListener {
                imageClick(absoluteAdapterPosition)
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutGalleryFullBinding.inflate(
            LayoutInflater.from(viewGroup.context), viewGroup, false
        )
        return ViewHolder(view, imageClick)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        with(dataSet[position]) {
            with(viewHolder) {
                imageView.apply {
                    load(url) {
                        crossfade(true)
                        crossfade(1000)
                    }
                }
                textGroup.apply {
                    visibility = if (showMeta) {
                        View.VISIBLE
                    } else {
                        View.GONE
                    }
                }
                textTitle.text = title
                textDescription.text = explanation
                textDate.text = context.getString(R.string.date, date)
                textAuthor.text = copyright
            }
        }
    }

    override fun getItemCount() = dataSet.size
}