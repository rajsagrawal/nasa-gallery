package com.raj.agrawal.nasagallery.data

import android.content.res.Resources
import com.raj.agrawal.nasagallery.R
import com.raj.agrawal.nasagallery.data.entities.GalleryResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.withContext
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader

class ImagesDataSource(private val resources: Resources) {

    private val businessLogic by lazy {
        BusinessLogic()
    }

    private val _galleryState = MutableStateFlow<GalleryResponse?>(null)

    val galleryState: StateFlow<GalleryResponse?>
        get() = _galleryState

    suspend fun fetchGallery() = withContext(Dispatchers.IO) {
        _galleryState.value = try {
            val jsonString = BufferedReader(
                InputStreamReader(BufferedInputStream(resources.openRawResource(R.raw.data)))
            ).use { it.readText() }
            businessLogic.parseAndReturnLatestData(jsonString)
        } catch (exception: Exception) {
            null
        }
    }
}