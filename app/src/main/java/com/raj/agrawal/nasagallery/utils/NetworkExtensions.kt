package com.raj.agrawal.nasagallery.utils

import android.content.Context
import android.net.ConnectivityManager

/**
 * Actual test if network is working
 * will use google DNS server by default
 *
 * @return
 */
fun Context.isOnline(): Boolean {
    val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork != null && activeNetwork.isConnected
}