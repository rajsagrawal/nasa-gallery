package com.raj.agrawal.nasagallery.data

import android.app.Application
import androidx.lifecycle.*
import com.raj.agrawal.nasagallery.data.entities.GalleryResponse
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class ImagesViewModel(val app: Application) : AndroidViewModel(app) {

    private val dataSource by lazy { ImagesDataSource(app.resources) }

    private val _galleryLiveData = MutableLiveData<GalleryResponse?>()

    /**
     * Observe livedata for list of images
     */
    val imagesLiveData: LiveData<GalleryResponse?>
        get() {
            viewModelScope.launch {
                dataSource.galleryState.collectLatest {
                    _galleryLiveData.postValue(it)
                }
            }
            return _galleryLiveData
        }

    /**
     * Launch this function to fetch the list of images
     */
    fun getImages() {
        viewModelScope.launch {
            dataSource.fetchGallery()
        }
    }
}