package com.raj.agrawal.nasagallery.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.SimpleItemAnimator
import com.raj.agrawal.nasagallery.data.ImagesViewModel
import com.raj.agrawal.nasagallery.databinding.FragmentFullGalleryBinding

class FullImageGalleryFragment : Fragment() {

    private lateinit var mBinding: FragmentFullGalleryBinding

    private val viewModel: ImagesViewModel by activityViewModels()

    private val args: FullImageGalleryFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentFullGalleryBinding.inflate(
            layoutInflater, container, false
        )

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.imagesLiveData.observe(viewLifecycleOwner) {
            it?.let { response ->
                mBinding.fullImageGalleryList.apply {
                    if (onFlingListener == null) {
                        PagerSnapHelper().attachToRecyclerView(this)
                    }
                    adapter = FullGalleryAdapter(response) { position ->
                        response[position].showMeta = !response[position].showMeta
                        adapter?.notifyItemChanged(position, false)
                    }
                    (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
                    scrollToPosition(args.imagePosition)
                }
            }
        }
    }
}