package com.raj.agrawal.nasagallery.data

import com.google.gson.Gson
import com.raj.agrawal.nasagallery.data.entities.GalleryResponse
import com.raj.agrawal.nasagallery.utils.fromJson
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class BusinessLogic {
    /**
     * Takes a json-string and returns a deserialized object.
     * Also sorts list items with latest images first.
     */
    internal fun parseAndReturnLatestData(jsonString: String): GalleryResponse? {
        val dateTimeFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern(DATE_FORMAT)
        val response = Gson().fromJson<GalleryResponse?>(jsonString)
        response?.sortByDescending {
            LocalDate.parse(it.date, dateTimeFormatter)
        }
        return response
    }

    companion object {
        private const val DATE_FORMAT = "yyyy-MM-dd"
    }
}