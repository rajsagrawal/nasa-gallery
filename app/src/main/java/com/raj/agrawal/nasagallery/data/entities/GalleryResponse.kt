package com.raj.agrawal.nasagallery.data.entities

import com.google.gson.annotations.SerializedName
import androidx.annotation.Keep

class GalleryResponse : ArrayList<GalleryResponse.GalleryResponseItem>(){
    @Keep
    data class GalleryResponseItem(
        @SerializedName("copyright")
        val copyright: String = "",
        @SerializedName("date")
        val date: String = "",
        @SerializedName("explanation")
        val explanation: String = "",
        @SerializedName("hdurl")
        val hdurl: String = "",
        @SerializedName("media_type")
        val mediaType: String = "",
        @SerializedName("service_version")
        val serviceVersion: String = "",
        @SerializedName("title")
        val title: String = "",
        @SerializedName("url")
        val url: String = "",
        var showMeta: Boolean = false
    )
}