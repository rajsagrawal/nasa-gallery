package com.raj.agrawal.nasagallery.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.raj.agrawal.nasagallery.data.entities.GalleryResponse
import com.raj.agrawal.nasagallery.databinding.LayoutGalleryThumbBinding

class ThumbGalleryAdapter(private val dataSet: GalleryResponse, private val imageClick: (position: Int) -> Unit) :
    RecyclerView.Adapter<ThumbGalleryAdapter.ViewHolder>() {

    class ViewHolder(
        binding: LayoutGalleryThumbBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        val imageView = binding.imageThumb
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutGalleryThumbBinding.inflate(
            LayoutInflater.from(viewGroup.context), viewGroup, false
        )
        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.imageView.load(dataSet[position].url) {
            crossfade(true)
            allowRgb565(true)
            crossfade(1000)
        }
        viewHolder.imageView.setOnClickListener {
            imageClick.invoke(position)
        }
    }

    override fun getItemCount() = dataSet.size
}