package com.raj.agrawal.nasagallery.utils

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.Fragment

/**
 * Use with a context.
 * Provide a string resource to show as a short-toast.
 */
fun Context.shortToast(strId: Int) {
    Toast.makeText(
        this,
        resources.getString(strId),
        Toast.LENGTH_SHORT
    ).show()
}

/**
 * Use with a context.
 * Provide a string resource to show as a long-toast.
 */
fun Context.longToast(strId: Int) {
    Toast.makeText(
        this,
        resources.getString(strId),
        Toast.LENGTH_LONG
    ).show()
}

/**
 * Use with a Fragment.
 * Provide a string resource to show as a short-toast.
 */
fun Fragment.shortToast(strId: Int) {
    context?.shortToast(strId)
}

/**
 * Use with a fragment.
 * Provide a string resource to show as a long-toast.
 */
fun Fragment.longToast(strId: Int) {
    context?.longToast(strId)
}