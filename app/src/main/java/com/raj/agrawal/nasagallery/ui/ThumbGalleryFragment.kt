package com.raj.agrawal.nasagallery.ui

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.raj.agrawal.nasagallery.R
import com.raj.agrawal.nasagallery.data.ImagesViewModel
import com.raj.agrawal.nasagallery.databinding.FragmentThumbGalleryBinding
import com.raj.agrawal.nasagallery.utils.isOnline

class ThumbGalleryFragment : Fragment() {

    private lateinit var mBinding: FragmentThumbGalleryBinding

    private val viewModel: ImagesViewModel by activityViewModels()

    private val snackBar by lazy {
        Snackbar.make(
            mBinding.root, R.string.no_internet, Snackbar.LENGTH_INDEFINITE
        ).setAction(R.string.retry) {
            Handler(Looper.getMainLooper()).postDelayed({
                fetchImages()
            }, 1000)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = FragmentThumbGalleryBinding.inflate(
            layoutInflater, container, false
        )
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetchImages()

        viewModel.imagesLiveData.observe(viewLifecycleOwner) {
            it?.let { response ->
                mBinding.galleryList.adapter = ThumbGalleryAdapter(response) { clickPosition ->
                    findNavController().navigate(
                        ThumbGalleryFragmentDirections
                            .actionGalleryFragmentToImageFragment()
                            .setImagePosition(clickPosition)
                    )
                }
            }
        }
    }

    private fun fetchImages() {
        if (requireContext().isOnline()) {
            viewModel.getImages()
            if (snackBar.isShown) {
                snackBar.dismiss()
            }
        } else {
            if (!snackBar.isShown) {
                snackBar.show()
            }
        }
    }
}