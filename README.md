# Raj Agrawal - Nasa Gallery App

This is an assignment submission for engineering position at Obvious.in. The app launches with a grid-view of images from NASA if internet is available. If connection fails, then a snackBar shows with an option to retry. By clicking any of the grid images you can see the image on full screen with related meta data.

## Instruction

To view the meta-data on full-image screen, tap on the image. To hide, tap anywhere on the screen again.

## App specifications

- mindSDK 26
- targetSDK 32
- Locked to portrait orientation
- Uses view-binding for easy view references
- Uses Coil library for rendering images
- Uses GSON library for parsing JSON strings into specific type objects
- Using the MVVM pattern where data is fetched from data-source (model) and wrapped into state flow, and then emitted back to to viewModel where it's converted into a live-data for lifecycle awareness, then eventually back to the view
- Uses Single Activity setup and allows user to toggle between two fragments using Navigation components and passing data using safeArgs
- Uses activityModels() in both fragments to share the data contained within the viewModel
- Written Unit and Espresso tests
- The full image screen uses the thumb-url for fast rendering, since the HD images take a long time to render

## Screenshots

<img src="https://i.imgur.com/kbEHqXm.png" width="250">
<img src="https://i.imgur.com/ZhnVju7.png" width="250">
<img src="https://i.imgur.com/1X9McWD.png" width="250">
